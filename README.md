# TiddlyWiki git file sync-adaptor plugin

## Motivation

I wanted to have my personal [TiddlyWiki][tw] files in a git repository. And I also
wanted the wiki in the regular one-file-per-tiddler format and not in a single
bundled file.

I also wanted it to be _offline-first_. Syncing when the network was up, but
keep working locally otherwise.

The existing git savers (GitHub, GitLab) save the wiki in a single file. And
work only when the network is up.


## Setup

- [Git](https://git-scm.com/) must be installed on the server.

- The wiki directory (with the `tidllers/*` files) must be a git repository,
  or a sub-directory of a repository. This plugin doesn't initializes the repo
  for you if there isn't any.

- [Simple-git](https://www.npmjs.com/package/simple-git) node package is used
  and must be installed to the same place as [TiddlyWiki][tw] itself (assuming it was installed as an npm package).

- This is a server side plugin for [TiddlyWiki][tw], and needs to be [setup accordingly](https://tiddlywiki.com/#Installing%20custom%20plugins%20on%20Node.js).


## Usage

Nothing special. Use your wiki as normal. In the background the tiddler file
changes will be committed to the git repo the wiki resides in.


[tw]: https://tiddylwiki.com