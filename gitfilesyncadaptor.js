/*\
title: $:/plugins/myrelly/gitfilesyncadaptor/gitfilesyncadaptor.js
type: application/javascript
module-type: syncadaptor

This syncadaptor dispatches to filesystem sync adaptor then commits major changes of tiddler
files into a git repository as well.

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

// This is a server only plugin
if(!$tw.node) {
	// Not exporting anything
	return;
}

var logger = new $tw.utils.Logger("GitFileSyncAdaptor");

var openGitRepo;
try {
	openGitRepo = require("simple-git");
} catch (error) {
	logger.alert("Required npm package 'simple-git' not found.\n  " + error);
	return;
}

if(!$tw.modules.titles["$:/plugins/tiddlywiki/filesystem/filesystemadaptor.js"]) {
	logger.alert("Required plugin filesystemadaptor not found.");
	return;
}

var FileSystemAdaptor = require("$:/plugins/tiddlywiki/filesystem/filesystemadaptor.js")
						    .adaptorClass;

function GitFileSyncAdaptor(options) {
	this.wiki = options.wiki;
	this.logger = logger;
	this.filesystem = new FileSystemAdaptor(options);
	this.gitRepo = openGitRepo($tw.boot.wikiTiddlersPath);
}

GitFileSyncAdaptor.prototype.name = "gitfilesyncadaptor";

GitFileSyncAdaptor.prototype.isReady = function() {
	return this.filesystem.isReady();
};

GitFileSyncAdaptor.prototype.getTiddlerInfo = function(tiddler) {
	return this.filesystem.getTiddlerInfo(tiddler);
};

GitFileSyncAdaptor.prototype.getTiddlerFileInfo = function(tiddler,callback) {
	this.filesystem.getTiddlerFileInfo(tiddler,callback);
};

GitFileSyncAdaptor.prototype.saveTiddler = function(tiddler,callback) {
	var gitRepo = this.gitRepo;
	this.filesystem.saveTiddler(
		tiddler,
		function(err,adaptorInfo,revision) {
			if (err) {
				callback(err);
				return;
			}

			var title = tiddler.fields.title;
			if (isTiddlerWanted($tw.wiki, title)) {
				var fileInfo = $tw.boot.files[title];
				stageTiddler("Edit", gitRepo, fileInfo);
				commit($tw.wiki, gitRepo, "Edit", title);
			}

			callback(null,adaptorInfo,revision);
		});
};

GitFileSyncAdaptor.prototype.loadTiddler = function(title,callback) {
	this.filesystem.loadTiddler(title,callback);
};

GitFileSyncAdaptor.prototype.deleteTiddler = function(title,callback,options) {
	var gitRepo = this.gitRepo;
	this.filesystem.deleteTiddler(
		title,
		function(err) {
			if(err) {
				callback(err);
				return;
			}

			if (isTiddlerWanted($tw.wiki, title)) {
				var fileInfo = $tw.boot.files[title];
				stageTiddler("Delete", gitRepo, fileInfo);
				commit($tw.wiki, gitRepo, "Delete", title);
			}

			callback(null);
		},
		options);
};

function isTiddlerWanted(wiki, title) {
	if(!title) {
		return false;
	}

	var ignorePattern = new RegExp(wiki.getTiddlerText("$:/config/GitFileSyncAdaptor/IgnorePattern"));
	var isIgnored = ignorePattern.test(title); 

	return !isIgnored;
}

function stageTiddler(action, gitRepo, fileInfo) {
	if (!fileInfo) {
		return;
	}

	gitRepo.add(fileInfo.filepath);
	logger.log("Stage " + action + " of '" + fileInfo.filepath + "'");

	if (fileInfo.hasMetaFile) {
		gitRepo.add(fileInfo.filepath + ".meta");
		logger.log("Stage " + action + " of '" + fileInfo.filepath + ".meta'");
	}
}

function commit(wiki, gitRepo, action, tiddlerTitle) {
	var commitMessageTemplate = wiki.getTiddlerText("$:/config/GitFileSyncAdaptor/CommitMessageTemplate");
	var commitMessage = commitMessageTemplate
		.replace("{Action}", action)
		.replace("{action}", action.toLocaleLowerCase())
		.replace("{TiddlerTitle}", tiddlerTitle);

	var options = {};
	var author = wiki.getTiddlerText("$:/config/GitFileSyncAdaptor/CommitAuthor") || "";
	if(author) {
		if (!new RegExp("^\\s*[\"'].*[\"']\\s*$").test(author)) {
			author = "\"" + author.trim() + "\"";
		}
		options["--author"] = author;
	}

	gitRepo.commit(commitMessage, options);
	logger.log("Commit \"" + commitMessage + "\"");
}


exports.adaptorClass = GitFileSyncAdaptor;

})();
